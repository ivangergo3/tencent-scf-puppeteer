'use strict';

const puppeteer = require('puppeteer');

exports.main_handler = async (event, context) => {
    const browser = await puppeteer.launch({
        args: ['--no-sandbox'],
    });
    const page = (await browser.pages())[0];
    await page.goto(
        'http://www.baidu.com/', {
        waitUntil: 'load', 
        timeout: 20000,
    });
    const title = await page.title();
    await browser.close();
    return {
        title: title,
    };
};
